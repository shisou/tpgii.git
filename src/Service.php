<?php

namespace shisou\tpgii;

use think\facade\Route;

class Service extends \think\Service
{
    public function boot()
    {
        $this->commands([
            'tpgii' => command\Tpgii::class
        ]);

        Route::get('tpgii', "\\shisou\\tpgii\\controller\\Tpgii@index");
        Route::post('tpgii/model', "\\shisou\\tpgii\\controller\\Tpgii@model");
    }
}
