<?php

namespace shisou\tpgii\controller;

use think\facade\Console;
use think\facade\Db;
use think\helper\Arr;

class Tpgii extends _Controller
{
    public function index()
    {

        return view(GII_VIEW_PATH);
    }

    public function model()
    {

        return json(["code" => 200, "msg" => "生成成功"]);
    }
}
