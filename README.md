# tp6-gii

### 介绍

1. tpgii：根据excel生成model和sql,需要在app目录下有model目录,生成的model继承AR类(AR类为自定义代码，文件存在则不生成)。
2. 生成的类文件以 ` // ---------- Custom code below ----------` 为分界线，上面是自动生成代码，下面为自定义代码

### 安装

`composer require shisou/tpgii `

### 使用

1. 配置如下:
    * 新建db.xlsx放入到think目录下，格式如下图：
    * 命令自动生成`id,created_at,updated_at,status`，编写excel时请忽略

   > * `@表名 `   一对一关联
   > * `@表名@ `  一对多关联
   > * <img src="./doc/db.jpg" alt="20x20" style="zoom:25%;" />

   > * 多态表：
   > * `&表名,&表名,&表名 `    多态一对一关联，关联几个表就写几个表
   > * `&表名&,&表名&,&表名& `   多态一对多关联，关联几个表就写几个表
   > * 多态表必须有字段 `当前表名_type` (存关联表的表名)，`当前表名_id`(存关联表的id)
   > * <img src="./doc/duotai.jpg" alt="20x20" style="zoom:25%;" />

2. tpgii运行步骤如下：
   > * 到 `think` 根目录先运行命令: `php think migrate:run` 数据库会有一个migrations表,这个是工具使用的表,不要修改
       如果想初次运行导入数据,到`think/database`目录下新建`init.php`文件:

      ```php
      <?php
          return [
              'setting'    => [
                  [
                      "name"       => "网站名称",
                      "key"        => "site_name",
                      "value"      => "XXX",
                  ],
                  [
                      "name"       => "网站关键词",
                      "key"        => "site_keywords",
                      "value"      => "XXX",
                  ],
              ],
              'admin'    => [
                  [
                      "username"   => "admin",
                      "pwd"        => "1234566544321",
                  ],
              ],
          ];
      ```
   > * 到 `think` 根目录运行命令: `php think tpgii` 会自动生成 model文件 和 迁移文件
   >* 可跟参数  `model`  只生成model
   >* 可跟参数 `sql`  只生成 `db.sql`

   > * 到 `think` 根目录运行命令: `php think migrate:run` 会自动修改数据库字段
   >* 可跟参数  `dataSql` 导入 database\data.sql文件 到数据库
   >* 到 `think` 根目录运行命令: `php think migrate:run 20240527114620`  将数据库恢复到指定版本。
